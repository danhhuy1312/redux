import {createStore, applyMiddleware} from 'redux';
import { LOAD_API_GRAMMAR } from '../actions/types';
import reducers from '../reducers'
import LoadAPIGrammar from '../reducers/LoadAPIGrammar';

const store = createStore(
    reducers,
);

export default store;