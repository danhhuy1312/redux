import {LOAD_API_GRAMMAR} from "./types"

export const loadAPIGrammar = (data)=>({ type: LOAD_API_GRAMMAR, data })
