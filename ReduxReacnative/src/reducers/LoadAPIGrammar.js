import {LOAD_API_GRAMMAR} from '../actions/types'

const data = {
    ListGrammar: null
};

export default function (state = data, action){
    switch (action.type) {
        case LOAD_API_GRAMMAR:
            data.ListGrammar = action.data;
            return {
                ...state,
                ListGrammar: action.data
            };
        default:
            return state;
    }
}

