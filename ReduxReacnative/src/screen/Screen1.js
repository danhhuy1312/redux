import React, { Component } from 'react';
import { View, Text, FlatList} from 'react-native';
import { connect } from 'react-redux';
import * as action from "../actions/index"
class Screen1 extends Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }

    componentDidMount(){
        this.LoadAPI();
    }

    LoadAPI() {
        return fetch('http://demo1913415.mockable.io/getVideoHot')
        .then(response => response.json()).then((responseJson) => {
            this.props.loadAPIGrammar(responseJson);
        }).catch((err) => {
            console.log('err', err)
        });
    }

    render(){
        return(
            <View>
                <FlatList
                    data={this.props.data.ListGrammar}
                    initialNumToRender={5}
                    renderItem={this.Render_Item_content}
                />
            </View>
        )
    }

    Render_Item_content = ({ item, index }) => {
        return (
            <View style={{ alignItems: "center" }}>
                <Text style={{ color: 'red' }}>{item.title}</Text>
            </View>
        )
    }
}

const mapStateToProps = state => ({
    data: state.LoadAPIGrammar
})

export default connect(mapStateToProps, action)(Screen1);